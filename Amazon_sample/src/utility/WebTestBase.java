package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class WebTestBase {
	
	public static WebDriver driver;
		
	public static void browserlaunch (String bname)
	{
		switch (bname.toLowerCase())
		{
		case "chrome":       
		
			System.setProperty("webdriver.chrome.driver", "/home/twilightuser/Documents/eclipse-workspace/Amazon_sample/Drivers/chromedriver");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		
		break;
		
		case "firefox":
			
			System.setProperty("webdriver.gecko.driver", "/home/twilightuser/Downloads/Selenium Package/geckodriver");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			
		break;
		
		default:
			
			System.setProperty("webdriver.chrome.driver", "/home/twilightuser/Downloads/Selenium Package/chromedriver");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		
			break;

		}
		
	}
	
	
	public static void applaunch (String url)
	{
		// applaunch
		driver.get(url);
	}
	
	public static void browserquit()
	{
		//browser close
		driver.quit();
	}
	
	public static void browserclose()
	{
		driver.close();
	}

}
