package pages;

import org.testng.annotations.Test;

import utility.WebTestBase;

public class amazon {
  @Test(priority=1)
  public void login() throws InterruptedException {
	  LoginPage.Login("918903365309", "Chandru@1711");
	  
  }
  @Test(priority=0)
  public void browser() throws InterruptedException {
	  WebTestBase.browserlaunch("chrome");
		WebTestBase.applaunch("https://www.amazon.in/");
  }
  @Test(priority=2)
  public void headphone() throws InterruptedException {
	  Select_Category.Search_category();
		Addtocart.cart();
  }
  @Test(priority=3)
  public void mackbook() throws InterruptedException {
	  Search.element("macbook pro");
		Viewcart.items();
  }
  @Test(priority=4)
  public void multiplesearch() throws InterruptedException {
	  Multiplesearch.searchele("iphone in mobile");
	  Multiplesearch.searchele("Android in mobile");
	  Multiplesearch.searchele("Android mobile cable");
	  Multiplesearch.searchele("iphone mobile cable");
	  Multiplesearch.searchele("iphone mobile case");
	  Multiplesearch.searchele("Android mobile case");
  }
  
  @Test(priority=5)
  public void logout() throws InterruptedException {
	  Logout.Logt();
}
}
