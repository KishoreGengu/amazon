package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.WebTestBase;


public class Addtocart extends WebTestBase {
	public static void cart() throws InterruptedException {
		// to add the product to cart
		driver.findElement(By.xpath("//span[text()='boAt BassHeads 100 in-Ear Headphones with Mic (Black)']")).click();
		Thread.sleep(5000);
		for(String handle: driver.getWindowHandles()) {
			driver.switchTo().window(handle);
		}
		driver.findElement(By.id("add-to-cart-button")).click();
		
	}
}
