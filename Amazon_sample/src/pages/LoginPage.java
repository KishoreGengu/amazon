package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import utility.WebTestBase;

public class LoginPage extends WebTestBase {

	public static void Login(String phonenumber, String password) throws InterruptedException 
	
	{
	//Login
		// selecting the signin button
		driver.findElement(By.xpath("//span[text()='Hello. Sign in']")).click();
		Thread.sleep(1000);
		// entering the mobile number
		driver.findElement(By.xpath("//div[@class='a-row a-spacing-base']//input[1]")).sendKeys(phonenumber);
		Thread.sleep(1000);
		//clicking on continue
		driver.findElement(By.xpath("//span[@class='a-button-inner']//input[1]")).click();
		Thread.sleep(1000);
		// entering the password
		driver.findElement(By.xpath("//div[@class='a-section a-spacing-large']//input[1]")).sendKeys(password);
		Thread.sleep(1000);
		// click on login
		driver.findElement(By.xpath("(//span[@class='a-button-inner']//input)[1]")).click();
		Thread.sleep(1000);
		System.out.println("Logged in successfully");
//		driver.switchTo().alert().sendKeys("Logged in successfully");

	}

}
