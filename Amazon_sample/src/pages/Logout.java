package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import utility.WebTestBase;

public class Logout extends WebTestBase{

	public static void Logt() throws InterruptedException {
		//to mouseover the settings option
		Actions a= new Actions(driver);
        WebElement ele=driver.findElement(By.xpath("(//a[@id='nav-link-accountList']//span)[2]"));
        a.moveToElement(ele).build().perform();
        Thread.sleep(2000);
        //to click on the sign out button
        driver.findElement(By.xpath("//a[@id='nav-item-signout']//span[1]")).click();
        System.out.println("Logged out successfully");
//        driver.switchTo().alert().sendKeys("Logged out successfully");
	}

}
