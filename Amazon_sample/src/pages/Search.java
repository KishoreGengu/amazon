package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utility.WebTestBase;

public class Search extends WebTestBase {
	public static void element(String search) throws InterruptedException {
	
	driver.findElement(By.id("twotabsearchtextbox")).sendKeys(search);
	driver.findElement(By.xpath("(//input[@class='nav-input'])[1]")).click();
	driver.findElement(By.xpath("(//a[@class='a-link-normal a-text-normal']//span)[2]")).click();
		for(String handle: driver.getWindowHandles()) {
		driver.switchTo().window(handle);
		}
	WebElement quantity = driver.findElement(By.xpath("(//select[@name='quantity'])[1]"));
	new Select(quantity).selectByIndex(1);
	driver.findElement(By.id("add-to-cart-button")).click();
	System.out.println("Headphone added successfully");
//	driver.switchTo().alert().sendKeys("Mackbook Pro added successfully to cart");
	}
}
