package mainclass;

import org.junit.Test;
import org.openqa.selenium.By;

import pages.Addtocart;
import pages.LoginPage;
import pages.Logout;
import pages.Multiplesearch;
import pages.Search;
import pages.Select_Category;
import pages.Viewcart;
import utility.WebTestBase;



public class Main extends WebTestBase {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
	//Go to amazon page
		WebTestBase.browserlaunch("chrome");
		WebTestBase.applaunch("https://www.amazon.in/");
		
	// login
		LoginPage.Login("918903365309", "Chandru@1711");

	// Select category
		Select_Category.Search_category();
		Addtocart.cart();
	
	// Search product
		Search.element("macbook pro");
		Viewcart.items();
		
	// Multi search
		Multiplesearch.searchele("iphone");
		Multiplesearch.searchele("Android");
		
	// logout
		Logout.Logt();
		WebTestBase.browserclose();

	}

}
